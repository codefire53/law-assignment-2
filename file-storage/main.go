package main

import (
	"fmt"
	"file-storage/controllers"
	"file-storage/middleware"
	"file-storage/services"
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"os"
)

func main() {
	router := mux.NewRouter()
	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/static/", http.StripPrefix("/static/",fs))

	fileService := services.FileServiceImpl{}
	fileController := controllers.FileController{
		FileService: fileService,
	}
	router.HandleFunc("/api/save-file", middleware.Authenticate(fileController.SaveFile)).Methods("POST")
	http.Handle("/", router)
	fmt.Println("Listening at port "+os.Getenv("server_port"))
	err := http.ListenAndServe(":"+os.Getenv("server_port"), nil)
	if err != nil {
		log.Fatal(err)
	}

}
