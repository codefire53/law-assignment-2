package services

import (
	"encoding/base64"
	"github.com/google/uuid"
	"os"
	"path/filepath"
)

type SaveOutcome struct {
	FileUrl string `json:"file_url""`
	Error string `json:"error""`
}
type FileServiceAbstract interface {
	SaveFile(filename string, file string) *SaveOutcome
}
type FileServiceImpl struct {
}

func (f FileServiceImpl) SaveFile(filename string, file string) *SaveOutcome {
	uuid := uuid.New()
	fileFormat := uuid.String()+"-"+filename

	decode, err := base64.StdEncoding.DecodeString(file)
	savePath, _ := filepath.Abs("./static/" + fileFormat)
	newFile, err := os.Create(savePath)
	defer newFile.Close()
	_, err = newFile.Write(decode)
	if err != nil {
		return &SaveOutcome{Error: err.Error()}
	}
	return &SaveOutcome{FileUrl: "http://localhost:8085/static/"+fileFormat}
}


