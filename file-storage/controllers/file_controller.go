package controllers

import (
	"encoding/json"
	"file-storage/services"
	"fmt"
	"net/http"
)

type FileRequest struct {
	File string `json:"file"`
	Filename string `json:"filename"`
}
type FileController struct {
	FileService services.FileServiceAbstract
}
func (f FileController) SaveFile(w http.ResponseWriter, r *http.Request) {
	req, _ := decodeFileRequest(r)
	fmt.Println("yes")
	ret  := f.FileService.SaveFile(req.Filename, req.File)
	WriteEncoded(w, ret)
}

func decodeFileRequest(r *http.Request) (*FileRequest, error) {
	req := new(FileRequest)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(req); err != nil {
		return nil, err
	}
	return req, nil
}
func WriteEncoded(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}