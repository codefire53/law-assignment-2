package main

import (
	"file-compressor/controllers"
	"file-compressor/middleware"
	"file-compressor/services"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
)

func main() {
	router := mux.NewRouter()
	compressService := services.CompressServiceImpl{}
	compressController := controllers.CompressController{
		CompressService: compressService,
	}
	router.HandleFunc("/api/compress-file", middleware.Authenticate(compressController.CompressFile)).Methods("POST")
	http.Handle("/", router)
	fmt.Println("Listening at port "+os.Getenv("server_port"))
	err := http.ListenAndServe(":"+os.Getenv("server_port"), nil)
	if err != nil {
		log.Fatal(err)
	}
}
