package controllers

import (
	"encoding/json"
	"file-compressor/services"
	"net/http"
)

type CompressRequest struct {
	File string `json:"file"`
	Filename string `json:"filename"`
}
type FileResponse struct {
	FileUrl string `json:"file_url"`
	Error string  `json:"error"`
}
type CompressController struct {
	CompressService services.CompressServiceAbstract
}
func (c CompressController) CompressFile(w http.ResponseWriter, r *http.Request) {
	req, err := decodeCompressRequest(r)
	if err != nil {
		WriteEncodedError(w, &services.CompressResponse{Error: err.Error()})
	}
	resp := c.CompressService.CompressFile(req.Filename, req.File, r.Header.Get("Authorization"))
	if resp.Error != "" {
		WriteEncodedError(w, resp)
	}
	WriteEncoded(w, resp)
}


func decodeCompressRequest(r *http.Request) (*CompressRequest, error) {
	req := new(CompressRequest)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(req); err != nil {
		return nil, err
	}
	return req, nil
}
func WriteEncodedError(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	json.NewEncoder(w).Encode(data)
}
func WriteEncoded(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}