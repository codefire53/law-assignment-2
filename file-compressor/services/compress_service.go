package services

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)
type CompressResponse struct {
	FileUrl string `json:"file_url"`
	Error string `json:"error"`
}

type CompressServiceAbstract interface {
	CompressFile(filename string, file string, token string)  (*CompressResponse)
}

type CompressServiceImpl struct {
}

func (c CompressServiceImpl) CompressFile(filename string, file string, token string) (*CompressResponse) {
	_ = decode(filename, file)
	compressedFileName := compress(filename)
	compressedFile := encode(compressedFileName)
	requestBody, _ := json.Marshal(map[string] string{
		"file": compressedFile,
		"filename": compressedFile,
	})
	resp, err := sendRequest(requestBody, token)
	if err != nil {
		return &CompressResponse{Error: err.Error()}
	}
	jsonResp := decodeJSON(resp)
	return &CompressResponse{FileUrl: jsonResp["file_url"]}
}

func sendRequest(requestBody []byte, token string) (*http.Response, error) {
	storageReq, err := http.NewRequest("POST", os.Getenv("storage_url")+"/api/save-file", bytes.NewBuffer(requestBody))
	if err != nil {
		return nil, err
	}
	storageReq.Header.Set("Authorization",  token)
	client := &http.Client{}
	resp, err := client.Do(storageReq)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func decodeJSON(resp *http.Response) map[string]string {
	var result map[string]string
	json.NewDecoder(resp.Body).Decode(&result)
	return result
}
func compress(filename string) (string) {
	f, _ := os.Open(filename)
	read := bufio.NewReader(f)
	data, _ := ioutil.ReadAll(read)
	outputFile := strings.Split(filename, ".")[1]
	outputFile = outputFile+".gz"
	f, _ = os.Create(outputFile)
	w := gzip.NewWriter(f)
	w.Write(data)
	w.Close()
	f.Close()
	os.Remove(filename)
	return outputFile
}
func encode(filename string) (string){
	buff, _ := ioutil.ReadFile(filename)
	compressedFile := base64.StdEncoding.EncodeToString(buff)
	os.Remove(filename)
	return compressedFile
}
func decode(fileName string, content string) error {
	decode, err := base64.StdEncoding.DecodeString(content)
	file, err := os.Create(fileName)
	defer file.Close()
	_, err = file.Write(decode)
	return err
}
