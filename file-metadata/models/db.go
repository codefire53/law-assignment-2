package models

import (
	"fmt"
	"os"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var db *gorm.DB

//InitDB ...
func InitDB() {
	conn, err := dbSetup()
	if err != nil {
		panic(err)
	}
	db = conn
	doMigration()
}

//GetDB ...
func GetDB() *gorm.DB {
	return db
}


func doMigration() {
	db.AutoMigrate(&Data{})
}

func dbSetup() (*gorm.DB, error){
	username := os.Getenv("db_user")
	password := os.Getenv("db_pass")
	dbName := os.Getenv("db_name")
	dbHost := os.Getenv("db_host")
	/*
		username := "mki"
		password := "9500mki"
		dbName := "goauth"
		dbHost := "localhost"
	*/
	dbParams := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable host=%s", username, password, dbName, dbHost)
	conn, err := gorm.Open("postgres", dbParams)
	if err != nil {
		return nil, err
	}
	return conn, err
}