package models

import "github.com/jinzhu/gorm"

type Data struct {
	gorm.Model
	File     string `gorm:"not null"`
	Url string `gorm: "not null"`
	Description string

}