package services

import (
	"bytes"
	"encoding/json"
	"file-metadata/models"
	"file-metadata/repositories"
	"fmt"
	"net/http"
	"os"
)

type DataListResponse struct {
	Data []DataInstanceResponse `json:"data"`
	Error string `json:"error"`
}
type DataResponse struct {
	Data DataInstanceResponse `json:"data"`
	Error string `json:"error"`
}
type DataInstanceResponse struct {
	ID uint `json:"id""`
	File string `json:"file"`
	Url string `json:"url"`
	Description string `json:"description,omitempty"`
}
type DeleteResponse struct {
	Message string `json:"message"`
	Error string `json:"error"`
}

type DataServiceAbstact interface {
	GetDatas() *DataListResponse
	UpdateDescription(id uint, description string) *DataResponse
	DeleteData(id uint) *DeleteResponse
	CreateData(filename, file, description ,token string) *DataResponse
}

type DataServiceImpl struct{
	DataRepository repositories.DataRepositoryAbstract
}
func (d DataServiceImpl) CreateData(filename, file, description, token string) *DataResponse {
	url, err := getFileUrl(filename, file, token)
	if err != nil {
		fmt.Println(err)
		return &DataResponse{Error: err.Error()}
	}
	data, err :=d.DataRepository.Create(&models.Data{File: filename, Url: url, Description: description,})
	if err != nil {
		fmt.Println(err)
		return &DataResponse{Error: err.Error()}
	}
	return &DataResponse{Data:
		DataInstanceResponse{
		ID: data.ID,
		File: data.File,
		Url: data.Url,
		Description: data.Description,
		},
	}
}

func (d DataServiceImpl) GetDatas() *DataListResponse {
	datas, err := d.DataRepository.GetAll()
	if err != nil {
		return &DataListResponse{Error: err.Error()}
	}
	dataResponses := make([]DataInstanceResponse, len(datas))
	for index, data := range datas  {
		dataResponses[index] = DataInstanceResponse{
			ID: data.ID,
			File: data.File,
			Url: data.Url,
			Description: data.Description,
		}
	}
	return &DataListResponse{Data: dataResponses}
}

func (d DataServiceImpl) DeleteData(id uint) *DeleteResponse {
	err := d.DataRepository.Delete(id)
	if err != nil {
		return &DeleteResponse{Error: err.Error()}
	}
	return &DeleteResponse{Message: "data has successfully deleted"}
}

func (d DataServiceImpl) UpdateDescription(id uint, description string) *DataResponse {
	oldData, err := d.DataRepository.GetByID(id)
	if err != nil {
		return &DataResponse{Error: err.Error()}
	}
	updatedData := &models.Data {
		Description: description,
		File: oldData.File,
		Url: oldData.Url,
	}
	updatedData, err = d.DataRepository.Update(updatedData)
	if err != nil {
		return &DataResponse{Error: err.Error()}
	}
	return &DataResponse{Data: DataInstanceResponse{
		ID: updatedData.ID,
		File: updatedData.File,
		Url: updatedData.Url,
		Description: updatedData.Description,
	}}
}
func getFileUrl(filename, file, token string) (string, error) {
	requestBody, _ := json.Marshal(map[string] string{
		"file": file,
		"filename": filename,
	})
	resp, err  := sendRequest(requestBody, token)
	if err != nil {
		return "", err
	}
	jsonResp := decodeJSON(resp)
	return jsonResp["file_url"], nil
}
func sendRequest(requestBody []byte, token string) (*http.Response, error) {
	storageReq, err := http.NewRequest("POST", os.Getenv("storage_url")+"/api/save-file", bytes.NewBuffer(requestBody))
	if err != nil {
		return nil, err
	}
	storageReq.Header.Set("Authorization",  token)
	client := &http.Client{}
	resp, err := client.Do(storageReq)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func decodeJSON(resp *http.Response) map[string]string {
	var result map[string]string
	json.NewDecoder(resp.Body).Decode(&result)
	return result
}