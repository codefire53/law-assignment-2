package main
import (
	"os"
	"fmt"
	"file-metadata/models"
	"github.com/gorilla/mux"
	"net/http"
	"file-metadata/repositories"
	"file-metadata/services"
	"file-metadata/middleware"
	"file-metadata/controllers"
)
func main() {
	router := mux.NewRouter()
	models.InitDB()
	dataRepository := repositories.DataRepositoryImpl{}
	dataService := services.DataServiceImpl{
		DataRepository: dataRepository,
	}
	dataController := controllers.DataController{
		DataService: dataService,
	}
	router.HandleFunc("/api/create-data",middleware.Authenticate(dataController.CreateData)).Methods("POST")
	router.HandleFunc("/api/get-datas", middleware.Authenticate(dataController.GetDatas)).Methods("GET")
	router.HandleFunc("/api/update-description/{id", middleware.Authenticate(dataController.UpdateData)).Methods("PUT")
	router.HandleFunc("/api/delete-data/{id}",middleware.Authenticate(dataController.DeleteData)).Methods("DELETE")
	http.Handle("/", router)
	fmt.Println("Listening at port "+os.Getenv("server_port"))
	err := http.ListenAndServe(":"+os.Getenv("server_port"), router)
	if err != nil {
		fmt.Print(err)
	}

}
