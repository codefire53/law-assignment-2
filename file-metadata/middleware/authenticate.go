package middleware

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"strings"
	"os"
	"fmt"
)

const (
	missingToken  = "Missing authorization token"
	invalidFormat = "Invalid token format"
	invalidToken = "Invalid token"
)

type AuthError struct {
	Code    string `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

func writeMiddleware(w http.ResponseWriter, data *AuthError) {
	w.WriteHeader(http.StatusForbidden)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func Authenticate(next http.HandlerFunc) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		tokenHeader := r.Header.Get("Authorization") // Grab the token from the header

		if tokenHeader == "" { // Token is missing, returns with error code 403 Unauthorized
			writeMiddleware(w, &AuthError{Code: "forbidden", Message: missingToken})
			return
		}

		splitted := strings.Split(tokenHeader, " ")
		if len(splitted) != 2 || strings.ToLower(splitted[0]) != "bearer" {
			writeMiddleware(w, &AuthError{Code: "forbidden", Message: invalidFormat})
			return
		}

		tokenPart := splitted[1]
		requestBody, _ := json.Marshal(map[string] string{
			"token": tokenPart,
		})
		resp, err := http.Post(os.Getenv("oauth_url")+"/api/verify", "application/json", bytes.NewBuffer(requestBody))
		if err != nil {
			fmt.Println(err)
			writeMiddleware(w, &AuthError{Code: "forbidden", Message: invalidFormat})
			return
		}
		var result map[string]interface{}
		json.NewDecoder(resp.Body).Decode(&result)
		if result["valid"] == false {
			writeMiddleware(w, &AuthError{Code: "forbidden", Message: invalidToken})
			return
		}
		r = r.WithContext(context.WithValue(r.Context(), "UserID", result["user_id"]))
		next(w, r)
	}
}
