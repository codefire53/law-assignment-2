package controllers

import (
	"encoding/json"
	"file-metadata/services"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)
type CreateRequest struct {
	File string `json:"file"`
	Content string `json:"content"`
	Description string `json:"description"`
}

type UpdateRequest struct {
	Description string `json:"description"`
}

type DataController struct {
	DataService services.DataServiceAbstact
}

func (d DataController) GetDatas(w http.ResponseWriter, r *http.Request) {

	response:= d.DataService.GetDatas()

	if response.Error != "" {
		writeEncodedError(w, response)
		return
	}

	writeEncoded(w, response)
}



func (d DataController) UpdateData(w http.ResponseWriter, r *http.Request) {
	dataID := parseID(r)
	req, err := decodeUpdateRequest(r)
	if err != nil {
		writeEncodedError(w, &services.DataResponse{Error: "invalid request"})
		return
	}
	response := d.DataService.UpdateDescription(dataID, req.Description)
	if response.Error != "" {
		writeEncodedError(w, response)
		return
	}
	writeEncoded(w, response)
}

func (d DataController) DeleteData(w http.ResponseWriter, r *http.Request) {
	dataID := parseID(r)
	response := d.DataService.DeleteData(dataID)
	if response.Error != "" {
		writeEncodedError(w, response)
		return
	}
	writeEncoded(w, response)
}
func (d DataController) CreateData(w http.ResponseWriter, r *http.Request) {
	req, err := decodeCreateRequest(r)
	tokenHeader := r.Header.Get("Authorization")
	if err != nil {
		writeEncodedError(w, &services.DataResponse{Error: "invalid request"})
		return
	}
	response := d.DataService.CreateData(req.File, req.Content, req.Description, tokenHeader)
	if response.Error != "" {
		writeEncodedError(w, response)
		return
	}
	writeEncoded(w, response)
}
func decodeUpdateRequest(r *http.Request) (*UpdateRequest, error) {
	req := new(UpdateRequest)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(req); err != nil {
		return nil, err
	}
	return req, nil
}

func decodeCreateRequest(r *http.Request) (*CreateRequest, error) {
	req := new(CreateRequest)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(req); err != nil {
		return nil, err
	}
	return req, nil
}

func parseID(r *http.Request) uint {
	res, _ := strconv.ParseUint(mux.Vars(r)["id"], 10, 64)
	return uint(res)
}

func writeEncoded(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func writeEncodedError(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	json.NewEncoder(w).Encode(data)
}

