package repositories

import "file-metadata/models"

type DataRepositoryAbstract interface {
	GetByID(id uint) (*models.Data, error)
	GetAll() ([]models.Data, error)
	Create(review *models.Data) (*models.Data, error)
	Update(review *models.Data) (*models.Data, error)
	Delete(id uint) error

}

type DataRepositoryImpl struct {
}

func (repo DataRepositoryImpl) GetByID(id uint) (*models.Data, error) {
	data := &models.Data{}
	err := models.GetDB().Where("id = ?", id).First(data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}
func (repo DataRepositoryImpl) GetAll() ([]models.Data, error) {
	var datas []models.Data
	err := models.GetDB().Find(&datas).Error
	if err != nil {
		return nil, err
	}

	return datas, err
}

func (repo DataRepositoryImpl) Create(data *models.Data) (*models.Data, error) {
	err := models.GetDB().Create(data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (repo DataRepositoryImpl) Update(data *models.Data) (*models.Data, error) {
	err := models.GetDB().Model(&data).Updates(&data).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (repo DataRepositoryImpl) Delete(id uint) error {
	err := models.GetDB().Delete(&models.Data{}, id).Error
	return err
}

