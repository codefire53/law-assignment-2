package repositories

import "oauth/models"

type UserRepositoryAbstract interface {
	GetByID(id uint) (*models.User, error)
	GetByUsername(username string) (*models.User, error)
}

type UserRepositoryImpl struct {
}

func (u UserRepositoryImpl) GetByID(id uint) (*models.User, error) {
	user := &models.User{}
	err := models.GetDB().Where("id = ?", id).First(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (u UserRepositoryImpl) GetByUsername(username string) (*models.User, error) {
	user := &models.User{}
	err := models.GetDB().Where("username = ?", username).First(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}
