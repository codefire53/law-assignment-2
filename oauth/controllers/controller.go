package controllers

import (
	"encoding/json"
	"net/http"
	"oauth/services"
)
type AuthRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
type VerifRequest struct {
	Token string `json:"token"`
}

type OAuthController struct  {
	OAuthService services.OAuthServiceAbstract
}
func (o OAuthController) Authenticate(w http.ResponseWriter, r *http.Request) {
	req, err := decodeAuthenticateRequest(r)
	if err != nil {
		WriteError(w, &services.TokenResponse{Error:err.Error()})
		return
	}
	ret := o.OAuthService.Authenticate(&services.UserPayload{
		Username: req.Username,
		Password: req.Password,
	})
	if ret.Error != "" {
		WriteError(w, ret)
		return
	}
	WriteEncoded(w, ret)
}

func (o OAuthController) Verify(w http.ResponseWriter, r *http.Request) {
	req, err := decodeVerifRequest(r)
	if err != nil {
		WriteError(w, &services.VerifResponse{Error: err.Error()})
	}
	ret := o.OAuthService.Verify(&services.TokenPayload{
		Token: req.Token,
	})
	WriteEncoded(w, ret)
}

func WriteEncoded(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}
func WriteError(w http.ResponseWriter, data interface{}) {
	w.WriteHeader(http.StatusBadRequest)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}
func decodeAuthenticateRequest(r *http.Request) (*AuthRequest, error) {
	req := new(AuthRequest)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(req); err != nil {
		return nil, err
	}
	return req, nil
}

func decodeVerifRequest(r *http.Request) (*VerifRequest, error) {
	req := new(VerifRequest)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(req); err != nil {
		return nil, err
	}
	return req, nil
}
