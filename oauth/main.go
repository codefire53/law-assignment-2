package main
import (
	"os"
	"fmt"
	"oauth/models"
	"github.com/gorilla/mux"
	"net/http"
	"oauth/repositories"
	"oauth/services"
	"oauth/middleware"
	"oauth/controllers"
)
func main() {
	router := mux.NewRouter()
	models.InitDB()
	userRepository := repositories.UserRepositoryImpl{}
	tokenCreation := middleware.TokenCreationImpl{}
	oAuthService := services.OAuthServiceImpl{
		UserRepository: userRepository,
		TokenCreation: tokenCreation,
	}
	oAuthController := controllers.OAuthController{
		OAuthService: oAuthService,
	}
	router.HandleFunc("/api/authenticate", oAuthController.Authenticate).Methods("POST")
	router.HandleFunc("/api/verify", oAuthController.Verify).Methods("POST")
	http.Handle("/api", router)
	fmt.Println("Listening at port "+os.Getenv("server_port"))
	err := http.ListenAndServe(":"+os.Getenv("server_port"), router)
	if err != nil {
		fmt.Print(err)
	}

}
