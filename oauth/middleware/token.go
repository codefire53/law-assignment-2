package middleware

import (
	"time"
	"github.com/dgrijalva/jwt-go"
	"os"
)

type Claims struct {
	jwt.StandardClaims
	UserID uint
}


type TokenCreationImpl struct {
}

type ITokenCreation interface {
	CreateToken(userID uint) string
}

func (t TokenCreationImpl) CreateToken(userID uint) string {
	expiresAt := time.Now().AddDate(0,0,1)
	tk := &Claims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    "oauth",
			ExpiresAt: expiresAt.Unix(),
		},
		UserID: userID,
	}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(os.Getenv("token_secret")))
	return tokenString
}
