-- +migrate Up
INSERT INTO users(id, created_at, updated_at, username, password) VALUES ('1', '2021-02-15 08:00:00.38991+00', '2021-02-15 08:00:00.38991+00', 'admin', '$2y$10$COpRA67SUeEI9rESTvSU1.vZfm9/wbaWTfL9IsvszbAEZ5NdPhgJW');
SELECT SETVAL('users_id_seq', 1);

-- +migrate Down
DELETE FROM users WHERE id=1;
SELECT SETVAL('users_id_seq', 1, false);
