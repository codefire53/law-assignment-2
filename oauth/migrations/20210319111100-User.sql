-- +migrate Up

-- SEQUENCE: public.users_id_seq
CREATE SEQUENCE users_id_seq;

-- Table: public.users
CREATE TABLE users
(
    id integer NOT NULL DEFAULT nextval('users_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    username character varying(75) COLLATE pg_catalog."default" NOT NULL,
    password text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT users_pkey PRIMARY KEY (id),
    CONSTRAINT users_username_key UNIQUE (username)
);

-- Index: idx_users_deleted_at
CREATE INDEX idx_users_deleted_at
    ON users USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down
DROP INDEX idx_users_deleted_at;
DROP TABLE users;
DROP SEQUENCE users_id_seq;
