package models

import "github.com/jinzhu/gorm"

type User struct {
	gorm.Model
	Username     string `gorm:"type:varchar(75);not null;unique"`
	Password string `gorm: "not null"`
}