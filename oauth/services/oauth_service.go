package services

import (
	"github.com/dgrijalva/jwt-go"
	"os"
	"golang.org/x/crypto/bcrypt"
	"oauth/middleware"
	"oauth/repositories"
)

type UserPayload struct {
	Username string
	Password string
}

type TokenResponse struct {
	Token string `json:"token,omitempty"`
	Error string `json:"error, omitempty"`
}
type VerifResponse struct {
	Valid bool `json:"valid, omitempty"`
	UserId uint `json:"user_id, omitempty"`
	Error string `json:"error, omitempty"`
}
type TokenPayload struct {
	Token string
}
type OAuthServiceAbstract interface {
	Authenticate(payload *UserPayload) *TokenResponse
	Verify(payload *TokenPayload) *VerifResponse
}
type OAuthServiceImpl struct{
	UserRepository repositories.UserRepositoryAbstract
	TokenCreation middleware.ITokenCreation
}
func (o OAuthServiceImpl) Authenticate(payload *UserPayload) *TokenResponse {
	user, err := o.UserRepository.GetByUsername(payload.Username)
	if err != nil {
		return &TokenResponse{Error: "Username doesnt exist"}
	}
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(payload.Password))
	if err != nil {
		return &TokenResponse{Error: "Password doesnt match"}
	}
	return &TokenResponse{Token: o.TokenCreation.CreateToken(user.ID)}
}

func (o OAuthServiceImpl) Verify(payload *TokenPayload)  *VerifResponse {
	if payload.Token == "" {
		return &VerifResponse{Valid: false}
	}
	tk := &middleware.Claims{}
	token, err := jwt.ParseWithClaims(payload.Token, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("token_secret")), nil
	})
	if token == nil || !token.Valid {
		return &VerifResponse{Valid: false}
	}
	_, err = o.UserRepository.GetByID(tk.UserID)
	if err != nil {
		return &VerifResponse{Valid: false}
	}
	return &VerifResponse{UserId: tk.UserID, Valid: true}
}